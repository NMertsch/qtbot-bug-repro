# Repro for [pytest-qt#550](https://github.com/pytest-dev/pytest-qt/issues/550)
Usage of `qtbot` fixture causes a segfault.

Please see the CI script and output for details.
